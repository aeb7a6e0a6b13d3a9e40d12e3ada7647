#define _GNU_SOURCE
#include <stdio.h>
int main() {
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  while ((read = getline(&line, &len, stdin)) > 0)
    if (read > 81)
      fwrite_unlocked(line, 1, read, stdout);
  return 0;
}
